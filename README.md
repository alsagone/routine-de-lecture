# Routine de lecture

Calcule la routine de lecture à avoir en fonction de :
- la page de début et de fin du livre
- la date du début et de fin de la lecture

Et permet de télécharger la routine dans un tableur au format .xlsx