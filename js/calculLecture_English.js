function nombrePages(a, b) {
    var myMin = Math.min(a, b);
    var myMax = Math.max(a, b);
    return myMax - myMin + 1;
}

function getPages() {
    var page1 = parseInt(document.getElementById("pageDepart").value, 10);
    var page2 = parseInt(document.getElementById("pageArrivee").value, 10);
    var pageDepart = Math.min(page1, page2);
    var pageArrivee = Math.max(page1, page2);
    return [pageDepart, pageArrivee];
}

function nombrePagesParJour(nbPages, nbJours) {
    var n;

    if (nbPages % nbJours == 0) {
        n = (nbPages / nbJours) >> 0; //Quotient de la division euclidienne
    } else {
        n = ((nbPages / nbJours) >> 0) + 1;
    }

    return n;
}

function pageDeFin(pageDepart, nbPages) {
    var n;

    if (nbPages != 1) {
        n = pageDepart + nbPages - 1;
    } else {
        n = pageDepart + nbPages;
    }
    return n;
}

function clear() {
    var list = document.querySelectorAll("#listeResultats > li");
    var i;

    for (i = 0; i < list.length; i++) {
        list[i].innerHTML = "";
    }

    return;
}

function getNbPagesDernierJour(nbPages, nbPagesParJour, nbJours) {
    n = nbPages - nbPagesParJour * (nbJours - 1);

    return n > 0 ? n : 0;
}

function calculerRoutine() {
    var dates = getDates();
    var date1 = dates[0];
    var date2 = dates[1];
    var nbJours;

    var pages = getPages();
    var pageDepart = pages[0];
    var pageArrivee = pages[1];
    var nbPages = nombrePages(pageDepart, pageArrivee);
    var nbPagesParJour, nbPagesDernierJour, totalPagesTheorique;

    var message, suiteMessage, premierJour, totalPages;
    var avantDernierJour;

    if (!estUneDateValide(date1)) {
        message =
            "Error on date 1 : " +
            dateToString(date1) +
            " is not a valid date..";
    } else if (!estUneDateValide(date2)) {
        message =
            "Error on date 2 : " +
            dateToString(date2) +
            " is not a valid date..";
    } else {
        nbJours = nombreJours(date1, date2) + 1;
        nbPagesParJour = nombrePagesParJour(nbPages, nbJours);
        totalPagesTheorique = nbPagesParJour * nbJours;
        premierJour =
            "Day 1 : " +
            pageDepart +
            " -> " +
            pageDeFin(pageDepart, nbPagesParJour);

        totalPages = "Total of pages : " + nombrePages(pageDepart, pageArrivee);
        if (
            totalPagesTheorique > nbPages &&
            nbJours < nbPagesParJour * nbJours
        ) {
            nbPagesDernierJour = getNbPagesDernierJour(
                nbPages,
                nbPagesParJour,
                nbJours
            );

            if (nbPagesDernierJour > 0) {
                avantDernierJour = supprimerJour(date2);
                message =
                    nbPagesParJour +
                    " pages per day starting " +
                    dateToString(date1) +
                    " to " +
                    dateToString(avantDernierJour);

                suiteMessage =
                    " and " +
                    nbPagesDernierJour +
                    " pages on the " +
                    dateToString(date2);

                if (nbPagesParJour < 2) {
                    message = message.replace("pages", "page");
                } else if (nbPagesDernierJour < 2) {
                    suiteMessage = suiteMessage.replace("pages", "page");
                }

                message += suiteMessage;
            } else {
                message =
                    nbPagesParJour +
                    " pages per day starting " +
                    dateToString(date1) +
                    " to " +
                    dateToString(date2);

                if (nbPagesParJour < 2) {
                    message = message.replace("pages", "page");
                }
            }
        } else {
            message =
                nbPagesParJour +
                " pages per day starting " +
                dateToString(date1) +
                " to " +
                dateToString(date2);

            if (nbPagesParJour < 2) {
                message = message.replace("pages", "page");
            }
        }
    }
    clear();
    ajouterResultat(message);
    ajouterResultat(premierJour);
    ajouterResultat(totalPages);
}

var aujourdHui = new Date();

function remplirAujourdHui1() {
    var jour = aujourdHui.getDate();
    var mois = aujourdHui.getMonth() + 1;
    var annee = aujourdHui.getFullYear();
    remplirDate(jour, mois, annee, "date1");
}

function remplirAujourdHui2() {
    var jour = aujourdHui.getDate();
    var mois = aujourdHui.getMonth() + 1;
    var annee = aujourdHui.getFullYear();
    remplirDate(jour, mois, annee, "date2");
}