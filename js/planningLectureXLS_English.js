//Partie SheetJS

var wb;
var pageDepart, pageArrivee, dateDepart, dateArrivee;
var offset, nbPages;

function ajouterColonne(contenu) {
    wb.Sheets["Schedule"] = XLSX.utils.aoa_to_sheet(contenu);
}

function initWorkBook() {
    wb = XLSX.utils.book_new();
    wb.Props = {
        Title: "Read schedule",
        Author: "Hakim ABDOUROIHAMANE",
        CreatedDate: new Date(),
    };

    wb.SheetNames.push("Schedule");
}

function getObjectif(pageDepart, nbPagesParJour) {
    var ob = pageDepart + nbPagesParJour - 1;

    if (ob > pageArrivee) {
        ob = pageArrivee;
    }

    return ob;
}

function getProgression(pageActuelle) {
    var p = ((pageActuelle - offset) * 100) / nbPages;
    return Number.parseFloat(p).toFixed(2) + " %";
}

function remplirXLS(dateDepart, pageDepart, nbPagesParJour, nbJours) {
    var date = dateDepart;
    var page = pageDepart;
    var contenu = [];
    var jour, objectif, progression;

    contenu.push([
        "Day",
        "Date",
        "Start page",
        "Objective",
        "Objective reached",
        "Progression",
    ]);

    for (jour = 1; jour <= nbJours; jour++) {
        objectif = getObjectif(page, nbPagesParJour);
        progression = getProgression(objectif);
        contenu.push([
            jour,
            dateToString(date),
            page,
            objectif,
            "",
            progression,
        ]);
        page = objectif + 1;
        date = ajouterJour(date);
    }

    ajouterColonne(contenu);
}

function construirePlanning() {
    var pages = getPages();
    pageDepart = pages[0];
    pageArrivee = pages[1];

    var dates = getDates();
    dateDepart = dates[0];
    dateArrivee = dates[1];

    var nbJours = nombreJours(dateDepart, dateArrivee) + 1;
    nbPages = nombrePages(pageDepart, pageArrivee);
    var nbPagesParJour = nombrePagesParJour(nbPages, nbJours);

    offset = pageDepart - 1;

    remplirXLS(dateDepart, pageDepart, nbPagesParJour, nbJours);
    return;
}

function s2ab(s) {
    var buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
    var view = new Uint8Array(buf); //create uint8array as viewer
    for (var i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff; //convert to octet
    return buf;
}

function isEmpty(str) {
    return !str || /^\s*$/.test(str) || !str.trim();
}

/* 
    Demande le titre du livre à l'utilisateur
    Et enlève les caractères interdits par Windows/Linux
    en tant que noms de fichier
*/
function promptTitreLivre() {
    var titreLivre = "";
    var regex = new RegExp('[<>:"/\\|?*]', "g");

    while (isEmpty(titreLivre)) {
        titreLivre = window.prompt("What is the title of the book ?");
    }

    return titreLivre.replace(regex, "");
}

function ulEmpty() {
    return $("ul").has("li").length == 0;
}

function telechargerPlanning() {
    if (ulEmpty()) {
        calculerRoutine();
    }

    initWorkBook();
    construirePlanning();
    var titreLivre = promptTitreLivre();
    var filename = titreLivre + " - Reading schedule.xlsx";

    var wbout = XLSX.write(wb, { bookType: "xlsx", type: "binary" });
    saveAs(
        new Blob([s2ab(wbout)], { type: "application/octet-stream" }),
        filename
    );
}